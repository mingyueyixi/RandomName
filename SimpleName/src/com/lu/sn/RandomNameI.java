package com.lu.sn;

public interface RandomNameI {
	public abstract String getFullName();
	public abstract String[] getSplitName();
	public abstract String getSimpleName();
}

