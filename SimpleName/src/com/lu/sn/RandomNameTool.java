package com.lu.sn;

import com.lu.sn.en.EnglishNameUtil;
import com.lu.sn.zh.SimpleZhNameUtil;

public class RandomNameTool {

	private static String getEnglishName(int flag) {
		if (flag == NameType.FULL_NAME) {				
			return EnglishNameUtil.getFullName();
		}			
		return EnglishNameUtil.getSimpleName();
	}

	public static String getName(Language language,int flag) {
		String name;
		switch (language) {
		case en:
			name = getEnglishName(flag);
			break;
		case zh:
			name = getZHName(flag);
			break;

		default:
			name = getEnglishName(flag);
			break;
		}
		return name;
	}

	private static String getZHName(int flag) {
		if (flag == NameType.FULL_NAME) {				
			return SimpleZhNameUtil.getFullName();
		}
		return SimpleZhNameUtil.getSimpleName();

	}
	private RandomNameTool() {
	}

}