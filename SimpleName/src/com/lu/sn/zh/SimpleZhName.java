package com.lu.sn.zh;

import java.io.UnsupportedEncodingException;
import java.util.Random;

import com.lu.sn.RandomNameI;
import com.lu.sn.util.Log;

public class SimpleZhName implements RandomNameI{

	private Random random;
	/**
	 * 构造方法
	 */
	public SimpleZhName() {
		random = new Random();
	}
	
	public String[] getFirstNameDesign() {
		return SourceZh.firstNameDesign;
	}
	/**
	 * @return 全名
	 */
	@Override
	public String getFullName() {
		return designFirstName()+designSimpleLastName();
	}

	@Override
	public String getSimpleName() {
		return designSimpleLastName();
	}

	/**
	 * @return 数组，[姓，名]
	 */
	@Override
	public String[] getSplitName() {
		String[] splitName = {designFirstName(),designSimpleLastName()};
		return splitName;
	}
	/**
	 * @return 数组中随机下标的字符串
	 */
	public String designFirstName() {
		int i = random.nextInt(SourceZh.firstNameDesign.length);//[0,n)
		return SourceZh.firstNameDesign[i];
	}
	/**
	 * 去掉了相当多的繁体字
	 * @return 随机生成一个汉字
	 */
	private String designSimpleLastName(){

		StringBuilder sBuilder = new StringBuilder();
		
		int lastLenght = 1+random.nextInt(2);
		
		for(int i=0;i<lastLenght;i++) {
			int hight = 176+random.nextInt(39);
			int low = 161+random.nextInt(93);
			byte[]han = new byte[2];
			
			han[0]= Integer.valueOf(hight).byteValue();
			han[1]= Integer.valueOf(low).byteValue();
			
			try {
				sBuilder.append(new String(han,"gbk"));
			} catch (UnsupportedEncodingException e) {
				Log.w(">>>", new Throwable(e));
			}
		}
		return sBuilder.toString();
	}
	
}

