package com.lu.sn.zh;

public class SourceZhJson {
	private SourceZhJson() {
		
	}
	public static String getSourceJson() {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("[");
		for (int i = 0; i < SourceZh.firstNameDesign.length; i++) {
			sBuilder.append("\""+SourceZh.firstNameDesign[i]+"\"");
			if (i!=SourceZh.firstNameDesign.length-1) {
				sBuilder.append(",");
			}
		}
		sBuilder.append("]");
		return sBuilder.toString();
	}
	public static String getFormatSourceJson() {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("[\n");
		for (int i = 0; i < SourceZh.firstNameDesign.length; i++) {
			sBuilder.append("    \""+SourceZh.firstNameDesign[i]+"\"");
			if (i!=SourceZh.firstNameDesign.length-1) {
				sBuilder.append(",\n");
			}
		}
		sBuilder.append("\n]");
		return sBuilder.toString();
	
	}
}
