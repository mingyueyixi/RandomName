package com.lu.sn.zh;

public class SimpleZhNameUtil {
	private final static class Holder{
		private static final SimpleZhName SIMPLE_ZH_NAME = new SimpleZhName();
	}
	/**
	 * @return 全名
	 */
	public static String getFullName() {
		return Holder.SIMPLE_ZH_NAME.getFullName();
	}
	public static String getSimpleName() {
		return Holder.SIMPLE_ZH_NAME.getSimpleName();
	}
	/**
	 * @return 数组，[姓，名]
	 */
	public static String[] getSplitName() {
		return Holder.SIMPLE_ZH_NAME.getSplitName();
	}
	private SimpleZhNameUtil() {
	}
	
}

