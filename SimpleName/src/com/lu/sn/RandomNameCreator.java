package com.lu.sn;

import com.lu.sn.util.Log;

/**
 * 构建工厂，构建name的实例
 * @author Yue
 *
 */
public class RandomNameCreator  implements RandomNameFactoryI{


	@SuppressWarnings("unchecked")
	@Override
	public <T extends RandomNameI> T factory(Class<T> cls) {

		T randomName = null;
		try {
			randomName = (T) Class.forName(cls.getName()).newInstance();
		} catch (Exception e) {
			Log.w(">>>"+e);
		}
		return randomName;
	}

}

