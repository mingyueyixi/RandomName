package com.lu.sn;

@FunctionalInterface
public interface RandomNameFactoryI {
	/**
	* 工厂方法
	* 创建一个产品对象，其输入参数类型可以自行设置
	*/
	public <T extends RandomNameI> T factory(Class<T> c);
	}


