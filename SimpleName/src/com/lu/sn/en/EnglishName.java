package com.lu.sn.en;

import java.util.Random;

import com.lu.sn.Language;
import com.lu.sn.RandomNameI;

/**
 * 英文的姓名，姓氏不很了解。。。目前统一返回简单名称
 */
public class EnglishName implements RandomNameI{

	private Random random;
	private int next;
	public EnglishName() {
		random = new Random();
	}
	@Override
	public String getFullName() {
		return insideName();
	}

	@Override
	public String getSimpleName() {
		return insideName();
	}

	@Override
	public String[] getSplitName() {
		return null;
	}
	public String getTranslateName(Language language) {
		String translateName = null; 
		switch (language) {
		case zh:
			if (next<TranslateZh.namesZh.length) {				
				translateName = TranslateZh.namesZh[next];
			}
			break;

		default:
			if (next<TranslateZh.namesZh.length) {				
				translateName = TranslateZh.namesZh[next];
			}
			break;
		}
		return translateName; 
	}
	private String insideName() {
		next = random.nextInt(SourceEn.namesEn.length);
		return SourceEn.namesEn[next];
	}
}

