package com.lu.sn.en;

import com.lu.sn.Language;

public class EnglishNameUtil {
	private static class Holder {
		private static final EnglishName NAME = new EnglishName(); 
	}
	public static String getFullName() {
		return Holder.NAME.getFullName();
	}
	public static String getSimpleName() {
		return Holder.NAME.getSimpleName();
	}
	/**
	 * @return 数组，[姓，名]
	 */
	public static String[] getSplitName() {
		return Holder.NAME.getSplitName();
	}
	public static String getTranslateName(Language language) {
		return Holder.NAME.getTranslateName(language);
	}
	private EnglishNameUtil() {
	}
}


