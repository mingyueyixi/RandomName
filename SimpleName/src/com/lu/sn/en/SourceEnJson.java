package com.lu.sn.en;

public class SourceEnJson {
	public static String getFormatSourceJson() {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("[\n");
		for (int i = 0; i < SourceEn.namesEn.length; i++) {
			sBuilder.append("    {\n");
			sBuilder.append("        \"en\": "+" \""+SourceEn.namesEn[i]+"\",\n");
			sBuilder.append("        \"zh\": "+" \""+TranslateZh.namesZh[i]+"\"\n");
			sBuilder.append("    }");
			if (i!=SourceEn.namesEn.length-1) {
				sBuilder.append(",\n");
			}
		}
		sBuilder.append("\n]");
		return sBuilder.toString();
	
	}
	public static String getSourceJson() {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("[");
		for (int i = 0; i < SourceEn.namesEn.length; i++) {
			sBuilder.append("{\"en\": "+"\""+SourceEn.namesEn[i]+"\"");
			sBuilder.append(", \"zh\": "+"\""+TranslateZh.namesZh[i] + "\"}");
			if (i!=SourceEn.namesEn.length-1) {
				sBuilder.append(",");
			}
		}
		sBuilder.append("]");
		return sBuilder.toString();
	}
	private SourceEnJson() {
	}
}
