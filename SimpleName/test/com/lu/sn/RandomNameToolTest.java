package com.lu.sn;

import org.junit.Test;

import com.lu.sn.en.EnglishName;
import com.lu.sn.en.EnglishNameUtil;
import com.lu.sn.zh.SimpleZhName;

public class RandomNameToolTest {
	@Test
	public void test() {
		int count = 25;
		for (int i = 0; i < count; i++) {
			String englishName = RandomNameTool.getName(Language.en,NameType.FULL_NAME);
			String translateName = EnglishNameUtil.getTranslateName(Language.zh);
			System.out.println(englishName + " --- "+ translateName);			
		}
		for (int i = 0; i < count; i++) {
			String chineseName = RandomNameTool.getName(Language.zh, NameType.FULL_NAME);
			System.out.println(chineseName);
		}
		
		RandomNameCreator randomNameCreator = new RandomNameCreator();
		EnglishName englishName = randomNameCreator.factory(EnglishName.class);
		SimpleZhName chineseName = randomNameCreator.factory(SimpleZhName.class);
		
		for(int i = 0;i<count;i++) {
			System.out.println(englishName.getFullName()+"---"+englishName.getTranslateName(Language.zh));
		}
		for(int i = 0;i<count;i++) {
			System.out.println(chineseName.getFullName());
		}
		
	}

}

