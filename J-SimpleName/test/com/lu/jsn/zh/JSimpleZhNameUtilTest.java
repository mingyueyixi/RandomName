package com.lu.jsn.zh;

import org.junit.Test;

import com.lu.jsn.JNameUtilI;
import com.lu.jsn.en.JEnglishNameUtil;

public class JSimpleZhNameUtilTest {

	@Test
	public void test() {
		
		System.out.println("----------英文名----------");
		
		JNameUtilI ji = JEnglishNameUtil.prepare();
		System.out.println(ji.getSimpleFullName());
		System.out.println(ji.getSimpleShortName());
		
		System.out.println(ji.getFullName());
		System.out.println(ji.getShortName());
		
		
		System.out.println("----------中文名----------");
		ji = JSimpleZhNameUtil.prepare();
		
		System.out.println(ji.getSimpleFullName());
		System.out.println(ji.getSimpleShortName());
		
		System.out.println(ji.getFullName());
		System.out.println(ji.getShortName());
		
		System.out.println("-----------更多测试-----------");
		
		int count4 = 0;
		int count6 = 0;
		int count5 = 0;
		int count3 = 0;
		int count2 = 0; 
		for(int i = 0;i<10;i++) {
			String[] names = JSimpleZhNameUtil.prepare().getSimpleOtherName();
			System.out.println(JSimpleZhNameUtil.prepare().getSimpleFullName() +"  别名：  "+ names[0]);
			
			if (names[0].length()==2) {
				count2 ++;
			}
			if (names[0].length()==3) {
				count3 ++;
			}
			if (names[0].length()==4) {
				count4 ++;
			}
			if (names[0].length()==5) {
				count5 ++;
			}
			if (names[0].length()==6) {
				count6 ++;
			}
		}
		System.out.println("次数：2Len>>"+ count2+ "  3len>>" + count3+ "  4len>>" + count4+ "  5len>>" + count5+ "  6len>>" + count6);
		
	}

}
