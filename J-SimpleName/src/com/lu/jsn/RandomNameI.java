package com.lu.jsn;

import com.lu.jsn.bean.FullHoldName;
import com.lu.jsn.bean.HoldName;

/**
 * @author Yue
 *
 */
public interface RandomNameI extends JNameResI{
	
	/**
	 * @return 全名
	 */
	public abstract String designSimpleFullName();	
	/**
	 * @return 姓氏
	 */
	public abstract String designSimpleFirstName();
	/**
	 * @return 名字
	 */
	public abstract String designSimpleLastName();
	/**
	 * @return 中间名
	 */
	public abstract String designSimpleMiddleName();
	/**
	 * @return 全名
	 */
	public abstract FullHoldName designFullName();
	/**
	 * @return 姓氏
	 */
	public abstract HoldName designFirstName();
	/**
	 * @return 中间名
	 */
	public abstract HoldName[] designMiddleName();
	/**
	 * @return 名字
	 */
	public abstract HoldName designLastName();
	/**
	 * @return 扩充的名字(别名、网名、昵称、字、号等)
	 */
	public abstract String[] designSimpleOtherName();
	/**
	 * @return 扩充的名字(别名、网名、昵称、字、号等)
	 */
	public abstract HoldName[] designOtherName();
	/**
	 * @return 简短的名字
	 */
	public abstract String designSimpleShortName();
	/**
	 * @return 简短的名字
	 */
	public abstract HoldName designShortName();
	
}

