package com.lu.jsn.zh;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;

import com.lu.jsn.RandomNameI;
import com.lu.jsn.bean.FullHoldName;
import com.lu.jsn.bean.HoldName;
import com.lu.jsn.util.FileUtil;
import com.lu.jsn.util.Log;
import com.lu.jsn.util.Resources;

public class JSimpleZhName implements RandomNameI{
	private ArrayList<String> firstNames;
	private Random random;
	public JSimpleZhName() {
		if (firstNames == null) {
			firstNames = new ArrayList<String>();
		}
		random = new Random();
	}
	@Override
	public HoldName designFirstName() {
		return new HoldName(designSimpleFirstName(),"");
	}
	@Override
	public FullHoldName designFullName() {
		return new FullHoldName(designFirstName(), designMiddleName(), designLastName());
	}
	
	@Override
	public HoldName designLastName() {
		return new HoldName(designSimpleLastName(), "");
	}
	@Override
	public HoldName[] designMiddleName() {
		return null;
	}
	
	/**
	 * @param len 长度
	 * @return 中文昵称
	 */
	public String designNickname(int len) {

		if (len < 1) {
			return "";
		}
		StringBuilder sBuilder = new StringBuilder();
		
		for(int i=0;i<len;i++) {
			int hight = 176+random.nextInt(39);
			int low = 161+random.nextInt(93);
			byte[]han = new byte[2];
			
			han[0]= Integer.valueOf(hight).byteValue();
			han[1]= Integer.valueOf(low).byteValue();
			
			try {
				sBuilder.append(new String(han,"gbk"));
			} catch (UnsupportedEncodingException e) {
				Log.w(">>>", new Throwable(e));
			}
		}
		return sBuilder.toString();
	}
	@Override
	public HoldName[] designOtherName() {
		HoldName holdName = new HoldName(designNickname(genNicknameLen()),"");
		return new HoldName[] {holdName};
	}
	private int genNicknameLen() {
		float fLen = 2+6*random.nextFloat();
		int size = 4;
		if (fLen < 3.5) {
			size = 2;
		}else if (fLen<4.5){
			size = 3;
		}else if (fLen<7.6){
			size = 4;
		}else if (fLen<7.8){
			size = 5;
		}else if (fLen<8){
			size = 6;
		}
		else {
			size = 4;
		}
		return size;
	}
	@Override
	public HoldName designShortName() {
		return designLastName();
	}
	/**
	 * @return 姓
	 */
	public String designSimpleFirstName(){	
		return firstNames.get(random.nextInt(firstNames.size()));
	}
	@Override
	public String designSimpleFullName() {
		return designSimpleFirstName()+designSimpleLastName();
	}
	/**
	 * 去掉了相当多的繁体字
	 * @return 随机生成一个汉字
	 */
	@Override
	public String designSimpleLastName(){

		StringBuilder sBuilder = new StringBuilder();
		
		int lastLenght = 1+random.nextInt(2);
		
		for(int i=0;i<lastLenght;i++) {
			int hight = 176+random.nextInt(39);
			int low = 161+random.nextInt(93);
			byte[]han = new byte[2];
			
			han[0]= Integer.valueOf(hight).byteValue();
			han[1]= Integer.valueOf(low).byteValue();
			
			try {
				sBuilder.append(new String(han,"gbk"));
			} catch (UnsupportedEncodingException e) {
				Log.w(">>>", new Throwable(e));
			}
		}
		return sBuilder.toString();
	}
	@Override
	public String designSimpleMiddleName() {
		return "";
	}
	@Override
	public String[] designSimpleOtherName() {
		return new String[] {designNickname(genNicknameLen())};
	}
	@Override
	public String designSimpleShortName() {
		return designSimpleLastName();
	}
	/**
	 * @return 姓集合
	 */
	public ArrayList<String> getFirstNames() {
		return firstNames;
	}
	/**
	 * 加载姓名
	 */
	@Override
	public void loadName() {
		String json = FileUtil.readBytesToString(Resources.getResInputStream("firstName_zh.json"),"utf-8");
		JSONArray jsonArray = new JSONArray(json);
		if (firstNames == null) {
			firstNames = new ArrayList<String>();
		}else {
			firstNames.clear();
		}

		for (int i = 0;i<jsonArray.length();i++) {
			firstNames.add(jsonArray.getString(i));
		}
	}
	@Override
	public void release() {
		if(firstNames!=null) {
			firstNames.clear();
		}
		firstNames = null;
	}
		
}
