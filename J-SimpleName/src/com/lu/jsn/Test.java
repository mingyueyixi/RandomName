package com.lu.jsn;

public class Test {

	public static void JSimpleNameTest() {
		System.out.println("---------------随机多语言姓名---------------");
		for(int i=0;i<50;i++) {			
			System.out.println(JRandomNameTool.getSimpleName());
		}
		System.out.println("--------------随机英文名----------------");
		for(int i=0;i<50;i++) {			
			System.out.println(JRandomNameTool.getSimpleName(Language.en));
		}
		System.out.println("--------------随机中文名----------------");
		for(int i=0;i<50;i++) {			
			System.out.println(JRandomNameTool.getSimpleName(Language.zh));
		}
	}
//	public static void main(String[] args) {
//		JSimpleNameTest();
//	}
}
