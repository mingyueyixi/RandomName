package com.lu.jsn;
import com.lu.jsn.en.JEnglishNameUtil;
import com.lu.jsn.zh.JSimpleZhNameUtil;

public class JRandomNameTool {

	/**
	 * @return 名字
	 */
	public static String getSimpleName(Language language) {
		JNameUtilI jRandomNameI = null;
		switch (language) {
		case en:
			jRandomNameI = JEnglishNameUtil.prepare();
			break;
		case zh:
			jRandomNameI = JSimpleZhNameUtil.prepare();
			break;
		default:
			jRandomNameI = JEnglishNameUtil.prepare();
			break;
		}
		return jRandomNameI.getSimpleFullName();
	}	
	/**
	 * @return 名字
	 */
	public static String getSimpleName() {
		int r = (int)(Math.random()*Language.values().length);		
		return getSimpleName(Language.values()[r]);
	}
	public static void clean() {
		JEnglishNameUtil.clean();
		JSimpleZhNameUtil.clean();
	}
	
}
