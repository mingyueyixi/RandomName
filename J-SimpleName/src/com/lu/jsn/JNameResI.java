package com.lu.jsn;

public interface JNameResI {
	/**
	 * 加载姓名
	 */
	void loadName();
	/**
	 * 释放资源
	 */
	void release();
}
