package com.lu.jsn.bean;

import java.io.Serializable;
import java.util.Arrays;

public class FullHoldName implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HoldName firstName;
	private HoldName[] middleName;
	private HoldName lastName;
	
	public FullHoldName() {
		super();
	}
	public FullHoldName(HoldName firstName, HoldName[] middleName, HoldName lastName) {
		super();
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
	}
	@Override
	public String toString() {
		return "FullHoldName [firstName=" + firstName + ", middleName=" + Arrays.toString(middleName) + ", lastName="
				+ lastName + "]";
	}
	public HoldName getFirstName() {
		return firstName;
	}
	public void setFirstName(HoldName firstName) {
		this.firstName = firstName;
	}
	public HoldName[] getMiddleName() {
		return middleName;
	}
	public void setMiddleName(HoldName[] middleName) {
		this.middleName = middleName;
	}
	public HoldName getLastName() {
		return lastName;
	}
	public void setLastName(HoldName lastName) {
		this.lastName = lastName;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
		
}
