package com.lu.jsn.bean;

import java.io.Serializable;
import java.io.ObjectOutputStream.PutField;

import javax.xml.soap.Text;

import com.lu.jsn.util.TextUtil;

public class HoldName implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * 原名
	 */
	private String origin;
	/**
	 * 默认译名
	 */
	private String translate;
	
	/**
	 * 英文译名
	 */
	private String TEn;
	/**
	 * 中文译名
	 */
	private String TZh;
	
	
	public HoldName() {
	}
	public HoldName(String origin, String translate) {
		super();
		this.origin = origin;
		this.translate = translate;
	}
	public String getOrigin() {
		return origin;
	}
	public String getTranslate() {
		return translate;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public void setTranslate(String translate) {
		this.translate = translate;
	}
	public String getTEn() {
		return TEn;
	}
	public void setTEn(String tEn) {
		TEn = tEn;
	}
	public String getTZh() {
		return TZh;
	}
	public void setTZh(String tZh) {
		TZh = tZh;
	}
	@Override
	public String toString() {
		return "HoldName [origin=" + origin + ", translate=" + translate + ", TEn=" + TEn + ", TZh=" + TZh + "]";
	}
	
	
	
	
	
}
