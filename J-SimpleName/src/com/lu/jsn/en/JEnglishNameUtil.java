package com.lu.jsn.en;

import com.lu.jsn.JNameUtilI;
import com.lu.jsn.RandomNameI;
import com.lu.jsn.bean.FullHoldName;
import com.lu.jsn.bean.HoldName;

public class JEnglishNameUtil implements JNameUtilI{
	private static enum Holder{
		H;
		private JEnglishNameUtil INSTANCE;
		private RandomNameI jRandomName;
		private Holder() {
			INSTANCE = new JEnglishNameUtil(); 
		}
		private void  prepareName() {
			if(jRandomName == null) {
				jRandomName = new JEnglishName();
				jRandomName.loadName();
			}
		}
		private void resetName() {
			if(jRandomName == null) {
				jRandomName = new JEnglishName();
			}
			jRandomName.loadName();
		}
		private void cleanName() {
			if (jRandomName==null) {
				return;
			}
			jRandomName.release();
			jRandomName = null;	
		}
	}
	/**
	 * 清理
	 */
	public static void clean() {
		Holder.H.cleanName();
	}
	/**
	 * 准备工作
	 * @return
	 */
	public static JEnglishNameUtil prepare() {
		Holder.H.prepareName();
		return Holder.H.INSTANCE;
	}
	/**
	 * 重置，重新从json中加载name
	 * @return
	 */
	public static JEnglishNameUtil reset() {
		Holder.H.resetName();
		return Holder.H.INSTANCE;
	}
	private JEnglishNameUtil() {
		
	}
	@Override
	public FullHoldName getFullName() {
		return Holder.H.jRandomName.designFullName();
	}
	@Override
	public HoldName getShortName() {
		return Holder.H.jRandomName.designShortName();
	}
	@Override
	public String getSimpleFullName() {
		return Holder.H.jRandomName.designSimpleFullName();
	}
	@Override
	public String getSimpleShortName() {
		return Holder.H.jRandomName.designSimpleShortName();
	}
	@Override
	public String[] getSimpleOtherName() {
		return Holder.H.jRandomName.designSimpleOtherName();
	}
	@Override
	public HoldName[] getOtherName() {
		return Holder.H.jRandomName.designOtherName();
	} 
}

