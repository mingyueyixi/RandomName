package com.lu.jsn.en;

import java.util.ArrayList;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.lu.jsn.RandomNameI;
import com.lu.jsn.bean.FullHoldName;
import com.lu.jsn.bean.HoldName;
import com.lu.jsn.util.FileUtil;
import com.lu.jsn.util.Resources;
import com.lu.jsn.util.TextUtil;

public class JEnglishName implements RandomNameI{

	private ArrayList<HoldName> lastNames;
	private Random random;
	public JEnglishName() {
		if (lastNames == null) {
			lastNames = new ArrayList<HoldName>();
		}
		random = new Random();
	}

	@Override
	public HoldName designFirstName() {
		//		int index = random.nextInt(lastNames.size());
//		Log.w(" designFirstName() do not some thing");

		return null;
	}
	@Override
	public FullHoldName designFullName() {
		HoldName firstName = designFirstName();
		HoldName[] mNames = designMiddleName();
		HoldName lastName = designLastName();
		return new FullHoldName(firstName,mNames,lastName);
	}


	@Override
	public HoldName designLastName() {
		int index = random.nextInt(lastNames.size());
		return lastNames.get(index);
	}

	@Override
	public HoldName[] designMiddleName() {
		return null;
	}

	@Override
	public HoldName[] designOtherName() {
		return null;
	}

	@Override
	public String designSimpleFirstName() {
		HoldName holdName = designFirstName();
		String firstName = (holdName !=null ? holdName.getOrigin(): "");
		return (firstName !=null ?firstName: "");
	}

	@Override
	public String designSimpleFullName() {

		StringBuilder fullName = new StringBuilder();
		fullName.append(designSimpleLastName());

		String middleName = designSimpleMiddleName();
		if (!TextUtil.isEmpty(middleName)) {
			fullName.append(" ");
			fullName.append(designSimpleMiddleName());
		}
		String firstName = designSimpleFirstName();

		if (!TextUtil.isEmpty(firstName)) {
			fullName.append(" ");
			fullName.append(firstName);
		}

		return fullName.toString();
	}

	@Override
	public String designSimpleLastName() {
		HoldName holdName = designLastName();
		String lastName = (holdName != null ? holdName.getOrigin() : "" );
		return (lastName !=null ? lastName: "");
	}

	@Override
	public String designSimpleMiddleName() {
		return "";
	}

	@Override
	public String[] designSimpleOtherName() {
		return null;
	}

	/**
	 * @return 名集合
	 */
	public ArrayList<HoldName> getLastNames() {
		return lastNames;
	}
	@Override
	public void loadName() {
		String json = FileUtil.readBytesToString(Resources.getResInputStream("lastName_en.json"),"utf-8");
		
		JSONArray jsonArray = new JSONArray(json);
		if (lastNames == null) {
			lastNames = new ArrayList<HoldName>();
		}else {
			lastNames.clear();
		}

		for (int i = 0;i<jsonArray.length();i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);		
			lastNames.add(new HoldName(jsonObject.getString("en"), jsonObject.getString("zh")));
		}
	}

	@Override
	public String designSimpleShortName() {
		return designSimpleLastName();
	}

	@Override
	public HoldName designShortName() {
		return designLastName();
	}

	@Override
	public void release() {
		if (lastNames!=null) {
			lastNames.clear();
		}
		lastNames = null;
	}



}

