package com.lu.jsn;

import com.lu.jsn.bean.FullHoldName;
import com.lu.jsn.bean.HoldName;

/**
 * @author Yue
 *
 * @param <H>
 * @param <F>
 */
public interface JNameUtilI <H extends HoldName,F extends FullHoldName> {
	/**
	 * @return 全名对象
	 */
	F getFullName();
	/**
	 * @return 字符串全名
	 */
	String getSimpleFullName();
	/**
	 * @return 简短名
	 */
	String getSimpleShortName();
	/**
	 * @return 简短名对象
	 */
	H getShortName();
	/**
	 * @return 别名数组
	 */
	String[] getSimpleOtherName();
	/**
	 * @return 别名数组
	 */
	H[] getOtherName();
	
	
}
