package com.lu.jsn.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
	private static final String log = ">>>";
	private static Logger mLogger;
	private static Logger init() {
		if (mLogger == null) {
			mLogger = Logger.getLogger(log);
		}
		return mLogger;
	}
	
	public static void w(String msg,Throwable thrown) {
		init();
		mLogger.log(Level.WARNING, msg, thrown);
	}
	public static void w(String msg) {
		init();
		mLogger.log(Level.WARNING, msg);;
	}
	public static void i(String msg,Throwable thrown) {
		init();
		mLogger.log(Level.INFO, msg,thrown);
	}
	public static void i(String msg) {
		init();
		mLogger.log(Level.INFO, msg);
	}
}
