package com.lu.jsn.util;


import java.io.InputStream;
import java.net.URL;

/**
 * 资源类
 * @author Yue
 *
 */
public class Resources {
	public static URL getResource(String name){
		return Resources.class.getClassLoader().getSystemResource("res/"+name);
    }
	public static InputStream getResInputStream(String name) {
		return Resources.class.getClassLoader().getSystemResourceAsStream("res/"+name);
	}
	public static URL getAssetResource(String name) {
		return Resources.class.getClassLoader().getSystemResource(name);
	}
	public static InputStream getAssetStream(String name) {
		return Resources.class.getClassLoader().getSystemResourceAsStream(name);
	}
}
