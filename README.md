# RandomName
支持生成中英文随机姓名，考虑支持更多语言

---

生成的库我直接复制在libs中了，貌似当时是为了方便测试，放dist目录比较合适。 

这其中

J-simplename.jar的资源文件是直接打包在jar中的，读取也是直接读。安卓上应该不支持。 

json-20170516.jar 是用来解析json，是依赖库。

simplename-20170918.jar 则是直接将人名资源写在类体中，一起编译的。这个代码写的略早，略有不同。

具体：

上：https://blog.csdn.net/Mingyueyixi/article/details/78022592

下：https://blog.csdn.net/Mingyueyixi/article/details/78066081
